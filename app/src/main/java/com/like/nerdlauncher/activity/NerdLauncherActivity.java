package com.like.nerdlauncher.activity;

import androidx.fragment.app.Fragment;

import com.like.nerdlauncher.activity.SingleFragmentActivity;
import com.like.nerdlauncher.fragment.NerdLauncherFragment;

public class NerdLauncherActivity extends SingleFragmentActivity {

    protected Fragment createFragment() {
        return NerdLauncherFragment.newInstance();
    }
}
